<div id="content">

<?php echo $this->Flash->render(); ?>

<div id="page-content" class="home-slider-content cabinet">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

		  <div class="col-md-12">
            <div class="page-content">

              <div class="product-details">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">

                  	<div class="row clearfix">
                      
                      <div class="col-sm-6 col-xs-12">
                        <div class="category-item">
                          <?php echo $this->Html->link('<i class="fa fa-users"></i>'.__('Работа с компаниями'), array('controller' => 'companies', 'action' => 'index'), array('escape' => false)); ?>
                        </div>
                      </div>

                      <div class="col-sm-6 col-xs-12">
                        <div class="category-item">
                          <?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Работа с категориями'), array('controller' => 'categories', 'action' => 'index'), array('escape' => false)); ?>
                        </div>
                      </div>

                    </div> <!-- end .row -->
                  </div> <!-- end .tabe-pane -->
                </div> <!-- end .tabe-content -->
              </div> <!-- end .product-details -->
            </div> <!-- end .page-content -->
          </div>
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
</div>