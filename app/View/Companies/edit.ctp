<?php echo $this->Flash->render(); ?>

<div id="page-content">
    <div class="container">
      <div class="page-content">
        <div class="contact-us">
		  <div class="row">

			<div class="col-md-12">
				<div class="contact-form">
					<?php echo $this->Form->create('Company'); ?>

						<h3><?php echo __('Редактирование компании'); ?></h3>
					<?php
						echo $this->Form->input('id');
						echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Название')));
						echo $this->Form->input('logo', array('label' => false, 'placeholder' => __('Ссылка на логотип')));
						echo $this->Form->input('short_desc', array('label' => false, 'placeholder' => __('Короткое описание')));
						echo $this->Form->input('full_desc', array('label' => false, 'placeholder' => __('Полное описание')));
					?>
						<div class="datetime-wrapper">
						<label for="date_found"><?php echo __('Дата основания'); ?></label>
					<?php
						$date = DateTime::createFromFormat("Y-m-d", $this->request->data['Company']['date_found']);
						echo $this->Form->dateTime('date_found', 'D', null, array('label' => false, 'empty' => false, 'data-selected' => $date->format("d")));
						echo $this->Form->dateTime('date_found', 'M', null, array('label' => false, 'empty' => false, 'data-selected' => $date->format("m")));
						echo $this->Form->dateTime('date_found', 'Y', null, array('label' => false, 'empty' => false, 'data-selected' => $date->format("Y"), 'minYear' => 0, 'maxYear' => date('Y')));
					?>
						</div>
					<?php
						echo $this->Form->input('contacts', array('label' => false, 'placeholder' => __('Контактные данные')));
						echo $this->Form->input('address', array('label' => false, 'placeholder' => __('Адрес')));
					?>
					<?php if ($this->request->data['Subcategory']): ?>
						<?php foreach ($this->request->data['Subcategory'] as $selected_subcategory): ?>

							<div class="row category-tag">
								<?php echo $this->Form->input('category_id', array('label' => __('Категория'), 'options' => $categories, 'selected' => $selected_subcategory['category_id'], 'data-selected' => $selected_subcategory['category_id'], 'div' => 'col-md-6', 'onchange' => 'load_input("companies/getSubcategoryInput/"+$(this).val())')); ?>
								<div class="additional">
									<?php
										echo $this->element('subcategory_select', array('subcategory_id' => $selected_subcategory['id'], 'added' => false));
									?>
								</div>
							</div>

						<?php endforeach; ?>
					<?php else: ?>

						<div class="row category-tag">
							<?php echo $this->Form->input('category_id', array('label' => __('Категория'), 'options' => $categories, 'div' => 'col-md-6', 'onchange' => 'load_input("companies/getSubcategoryInput/"+$(this).val())')); ?>
							<div class="additional"></div>
						</div>

					<?php endif; ?>
					
					<?php echo $this->Form->end(__('Готово')); ?>
				</div>
			</div>

          </div> <!-- end .row -->
        </div> <!-- end .contact-us -->

		<div class="actions blog-list">
			<h3><?php echo __('Действия'); ?></h3>
			<?php echo $this->Form->postLink('<i class="fa fa-trash-o"></i>'.__('Удалить'), array('action' => 'delete', $this->Form->value('Company.id')), array('confirm' => __('Вы действительно желаете удалить "%s"?', $this->Form->value('Company.name')), 'class' => 'post-read-more', 'escape' => false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Список компаний'), array('action' => 'index'), array('class' => 'post-read-more', 'escape' => false)); ?>
		</div>

      </div> <!-- end .page-content -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->