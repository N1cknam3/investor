<?php echo $this->Flash->render(); ?>

<div id="page-content">
    <div class="container">
      <div class="page-content">
        <div class="contact-us">
		  <div class="row">

			<div class="col-md-12">
				<div class="contact-form">
					<?php echo $this->Form->create('Company'); ?>

						<h3><?php echo __('Новая компания'); ?></h3>
					<?php
						echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Название')));
						echo $this->Form->input('logo', array('label' => false, 'placeholder' => __('Ссылка на логотип')));
						echo $this->Form->input('short_desc', array('label' => false, 'placeholder' => __('Короткое описание')));
						echo $this->Form->input('full_desc', array('label' => false, 'placeholder' => __('Полное описание')));
					?>
						<div class="datetime-wrapper">
						<label for="date_found"><?php echo __('Дата основания'); ?></label>
					<?php
						echo $this->Form->dateTime('date_found', 'D', null, array('label' => false, 'empty' => false, 'selected' => date('d'), 'data-placeholder-selected' => date('d')));
						echo $this->Form->dateTime('date_found', 'M', null, array('label' => false, 'empty' => false, 'selected' => date('m'), 'data-placeholder-selected' => date('m')));
						echo $this->Form->dateTime('date_found', 'Y', null, array('label' => false, 'empty' => false, 'selected' => date('Y'), 'data-placeholder-selected' => date('Y'), 'minYear' => 0, 'maxYear' => date('Y')));
					?>
						</div>
					<?php
						echo $this->Form->input('contacts', array('label' => false, 'placeholder' => __('Контактные данные')));
						echo $this->Form->input('address', array('label' => false, 'placeholder' => __('Адрес')));
					?>
					<div class="row category-tag">
						<?php echo $this->Form->input('category_id', array('label' => __('Категория'), 'options' => $categories, 'data-selected' => @$category_id, 'div' => 'col-md-6', 'onchange' => 'load_input("companies/getSubcategoryInput/"+$(this).val())')); ?>
						<div class="additional">
							<?php echo $this->element('subcategory_select', array('added' => false)); ?>
						</div>
					</div>
					
					<?php echo $this->Form->end(__('Готово')); ?>
				</div>
			</div>

          </div> <!-- end .row -->
        </div> <!-- end .contact-us -->

		<div class="actions blog-list">
			<h3><?php echo __('Действия'); ?></h3>
			<?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Список компаний'), array('action' => 'index'), array('class' => 'post-read-more', 'escape' => false)); ?>
		</div>

      </div> <!-- end .page-content -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->