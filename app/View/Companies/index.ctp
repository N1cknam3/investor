<div id="content">

<?php echo $this->Flash->render(); ?>

<div id="page-content" class="home-slider-content cabinet">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

		  <div class="col-md-12">
            <div class="page-content">

              <div class="product-details-list my-list">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">

                  	<!-- Content goes here -->

                  	<h2><?php echo __('Список компаний'); ?></h2>
					<div class="row clearfix">
						<?php
							if (empty($companies))
								echo $this->element('placeholder_search', array('placeholder_text' => __('Не создано ни одной компании')));

							foreach ($companies as $company):
						?>

								<div class="col-sm-12 col-xs-12"><div class="single-product">
					                
					                <h4><?php echo h($company['Company']['name']); ?></h4>

					                <?php
					                	if ($company['Company']['priority'] == 0) {
					                		echo $this->Html->link('<i class="fa fa-square-o"></i>'.__('Установить приоритет'), array('action' => 'togglePriority', $company['Company']['id']), array('class' => 'read-more', 'escape' => false ));
					                	} else {
					                		echo $this->Html->link('<i class="fa fa-check-square"></i>'.__('Приоритет установлен'), array('action' => 'togglePriority', $company['Company']['id']), array('class' => 'read-more green', 'escape' => false ));
					                	}
					                ?>
					                <?php echo $this->Html->link('<i class="fa fa-search"></i>'.__('Просмотр'), array('action' => 'view', $company['Company']['id']), array('class' => 'read-more', 'escape' => false )); ?>

					                <?php
					                	foreach($languages as $language) {
					                		echo $this->Html->link(' <i class="fa fa-pencil-square-o"></i>'.__('Изменить')." $language", array('action' => 'edit', $company['Company']['id'], $language), array('class' => 'read-more', 'escape' => false ));
					                	}
					                ?>

									<?php echo $this->Form->postLink('<i class="fa fa-trash-o"></i>'.__('Удалить'), array('action' => 'delete', $company['Company']['id']), array('confirm' => __('Вы действительно желаете удалить "%s"?', $company['Company']['name']), 'class' => 'read-more', 'escape' => false)); ?>
				                
				                </div></div>
				        <?php endforeach; ?>
			        </div>

					<div class="row">
						<?php echo $this->element('paginator'); ?>
					</div>

					<div class="actions blog-list">
						<h3><?php echo __('Действия'); ?></h3>
						<?php echo $this->Html->link('<i class="fa fa-plus"></i>'.__('Добавить компанию'), array('action' => 'add'), array('class' => 'post-read-more', 'escape' => false)); ?>
					</div>

					<!-- End content -->

					</div> <!-- end .tabe-pane -->
                  
                </div> <!-- end .tabe-content -->

              </div> <!-- end .product-details -->
            </div> <!-- end .page-content -->
          </div>
          
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
</div>
