<div id="page-content">
    <div class="container">
      <div class="row">
        <div class="col-md-9 col-md-push-3">
          <div class="page-content company-profile">

            <div class="tab-content">
              <div class="tab-pane active" id="company-profile">
	              
	            <?php
		            echo '<div style="width: 150px; height: 150px; margin: 0 20px 20px; background-image: url('.$company['Company']['logo'].'); background-size: contain; background-repeat: no-repeat; background-position: 50% 50%; float: right; "></div>';
	            ?> 
	              
                <h2><?php echo h($company['Company']['name']); ?></h2>

                <?php
                  foreach ($company['Subcategory'] as $subcategory) {
                    echo '<h5><a href="'
                      .Router::url(array('controller' => 'main', 'action' => 'search/all/'.$subcategory['category_id'])).'">'.$categories_global[$subcategory['category_id']].',</a> <a href="'
                      .Router::url(array('controller' => 'main', 'action' => 'search/all/'.$subcategory['category_id'].'/'.$subcategory['id'])).'">'.$subcategory['name'].'</a></h5>';
                  }
                ?>
                
                <?php
// 					echo '<div style="width: 100%; height: 150px; background-image: url('.$company['Company']['logo'].'); background-size: contain; background-repeat: no-repeat; background-position: 50% 50%;"></div>';
				?>

<!--
                <div class="social-link text-right">
                  <ul class="list-inline">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
-->

                <div class="company-text">
                  <p style="text-align: justify;">
                    <strong><?php echo h($company['Company']['short_desc']); ?></strong>
                  </p>

                  <p style="text-align: justify;">
	                <?php echo h($company['Company']['full_desc']); ?>
                  </p>
                </div> <!-- end company-text -->



                
              </div> <!-- end .tab-pane -->

              <div class="tab-pane" id="company-product">
                <div class="company-product">

                  <h2 class="text-uppercase mb30">Our Products</h2>

                  <div class="sort-by">

                    <select class="" data-placeholder="-sort by-">
                      <option value="option1">Name</option>
                      <option value="option2">Tupe</option>
                      <option value="option3">Name</option>
                      <option value="option4">Type</option>
                    </select>

                  </div>

                  <div class="row">
                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->


                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/company-product.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Product Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                  </div> <!-- end .row -->
                </div> <!-- end .company-product -->
              </div> <!-- end .tab-pane -->

              <div class="tab-pane" id="company-portfolio">

                <div class="company-portfolio">
                  <h2 class="text-uppercase mb30">Our Portfolio</h2>

                  <div class="sort-by">

                    <select class="" data-placeholder="-sort by-">
                      <option value="option1">Name</option>
                      <option value="option2">Tupe</option>
                      <option value="option3">Name</option>
                      <option value="option4">Type</option>
                    </select>

                  </div>

                  <div class="row">
                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                    <div class="col-sm-4 col-xs-6">
                      <div class="single-product">
                        <figure>
                          <img src="img/content/post-img-9.jpg" alt="">

                          <figcaption>
                            <div class="bookmark">
                              <a href="#"><i class="fa fa-bookmark-o"></i> Bookmark</a>
                            </div>

                            <div class="read-more">
                              <a href="#"><i class="fa fa-angle-right"></i> Read More</a>
                            </div>

                          </figcaption>
                        </figure>

                        <h4><a href="#">Project Name</a></h4>

                        <h5><a href="#">Category</a></h5>
                      </div> <!-- end .single-product -->
                    </div> <!-- end .grid-layout -->

                  </div> <!-- end .row -->
                </div> <!-- end .company-portfolio -->
              </div> <!-- end .tab-pane -->

              <div class="tab-pane" id="company-events">

                <div class="company-events">

                  <h2 class="mb30">Events</h2>

                  <div class="post-with-image">
                    <div class="date-month">
                      <a href="#">
                        <span class="date">12</span>
                        <span class="month">Sep</span>
                      </a>
                    </div>

                    <div class="post-image">
                      <img src="img/content/blog-list-image1.jpg" alt="">
                    </div>

                    <h2 class="title"><a href="blog-post.html">This is Event title</a></h2>

                    <p class="event-place">
                      <a href="#"><i class="fa fa-globe"></i> Rome, Italy</a>
                      <a href="#"><i class="fa fa-calendar"></i> 12 Sep, 2014</a>
                      <a href="#"><i class="fa fa-clock-o"></i> 8 PM</a>
                    </p>

                    <p class="post">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                      suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                      Phasellus pharetra nulla ac diam. Quisque semper.
                    </p>

                    <p class="tag">
                      <i class="fa fa-tag"></i>
                      <a href="#">Good,</a>
                      <a href="#">Ui,</a>
                      <a href="#">Experience,</a>
                      <a href="#">Article</a>
                    </p>

                    <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                  </div> <!-- end .post-with-image -->

                  <div class="post-with-image">
                    <div class="date-month">
                      <a href="#">
                        <span class="date">12</span>
                        <span class="month">Sep</span>
                      </a>
                    </div>

                    <div class="post-image">
                      <img src="img/content/blog-list-image2.jpg" alt="">
                    </div>

                    <h2 class="title"><a href="#">This is Event title</a></h2>

                    <p class="event-place">
                      <a href="#"><i class="fa fa-globe"></i> Rome, Italy</a>
                      <a href="#"><i class="fa fa-calendar"></i> 12 Sep, 2014</a>
                      <a href="#"><i class="fa fa-clock-o"></i> 8 PM</a>
                    </p>

                    <p class="post">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                      suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                      Phasellus pharetra nulla ac diam. Quisque semper.
                    </p>

                    <p class="tag">
                      <i class="fa fa-tag"></i>
                      <a href="#">Good,</a>
                      <a href="#">Ui,</a>
                      <a href="#">Experience,</a>
                      <a href="#">Article</a>
                    </p>

                    <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                  </div> <!-- end .post-with-image -->

                  <div class="post-with-image">
                    <div class="date-month">
                      <a href="#">
                        <span class="date">12</span>
                        <span class="month">Sep</span>
                      </a>
                    </div>

                    <div class="post-image">
                      <img src="img/content/blog-list-image3.jpg" alt="">
                    </div>

                    <h2 class="title"><a href="#">This is Event title</a></h2>

                    <p class="event-place">
                      <a href="#"><i class="fa fa-globe"></i> Rome, Italy</a>
                      <a href="#"><i class="fa fa-calendar"></i> 12 Sep, 2014</a>
                      <a href="#"><i class="fa fa-clock-o"></i> 8 PM</a>
                    </p>

                    <p class="post">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                      suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                      Phasellus pharetra nulla ac diam. Quisque semper.
                    </p>

                    <p class="tag">
                      <i class="fa fa-tag"></i>
                      <a href="#">Good,</a>
                      <a href="#">Ui,</a>
                      <a href="#">Experience,</a>
                      <a href="#">Article</a>
                    </p>

                    <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                  </div> <!-- end .post-with-image -->




                  <div class="post-with-image">
                    <div class="date-month">
                      <a href="#">
                        <span class="date">12</span>
                        <span class="month">Sep</span>
                      </a>
                    </div>

                    <div class="post-image">
                      <img src="img/content/blog-list-image2.jpg" alt="">
                    </div>

                    <h2 class="title"><a href="#">This is Event title</a></h2>

                    <p class="event-place">
                      <a href="#"><i class="fa fa-globe"></i> Rome, Italy</a>
                      <a href="#"><i class="fa fa-calendar"></i> 12 Sep, 2014</a>
                      <a href="#"><i class="fa fa-clock-o"></i> 8 PM</a>
                    </p>

                    <p class="post">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                      suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                      Phasellus pharetra nulla ac diam. Quisque
                    </p>

                    <p class="tag">
                      <i class="fa fa-tag"></i>
                      <a href="#">Good,</a>
                      <a href="#">Ui,</a>
                      <a href="#">Experience,</a>
                      <a href="#">Article</a>
                    </p>

                    <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                  </div> <!-- end .post-with-image -->


                  <div class="post-with-image">
                    <div class="date-month">
                      <a href="#">
                        <span class="date">12</span>
                        <span class="month">Sep</span>
                      </a>
                    </div>

                    <div class="post-image">
                      <img src="img/content/blog-list-image1.jpg" alt="">
                    </div>

                    <h2 class="title"><a href="#">This is Event title</a></h2>

                    <p class="event-place">
                      <a href="#"><i class="fa fa-globe"></i> Rome, Italy</a>
                      <a href="#"><i class="fa fa-calendar"></i> 12 Sep, 2014</a>
                      <a href="#"><i class="fa fa-clock-o"></i> 8 PM</a>
                    </p>

                    <p class="post">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                      suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                      Phasellus pharetra nulla ac diam. Quisque semper.
                    </p>

                    <p class="tag">
                      <i class="fa fa-tag"></i>
                      <a href="#">Good,</a>
                      <a href="#">Ui,</a>
                      <a href="#">Experience,</a>
                      <a href="#">Article</a>
                    </p>

                    <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                  </div> <!-- end .post-with-image -->

                </div> <!-- end .company-events -->
              </div> <!-- end .tab-pane -->

              <div class="tab-pane" id="company-blog">

                <div class="company-blog">

                  <h2 class="text-uppercase mb30">Our Blog</h2>

                  <div class="blog-list">

                    <div class="post-with-image">
                      <div class="date-month">
                        <a href="#">
                          <span class="date">12</span>
                          <span class="month">Sep</span>
                        </a>
                      </div>

                      <div class="post-image">
                        <img src="img/content/blog-list-image1.jpg" alt="">
                      </div>

                      <h2 class="title"><a href="blog-post.html">This is Post Title</a></h2>

                      <p class="user">
                        <a href="#"><i class="fa fa-user"></i> Admin</a>
                        <a href="#"><i class="fa fa-folder-open-o"></i> Design</a>
                        <a href="#"><i class="fa fa-comments-o"></i> 2 Comments</a>
                      </p>

                      <p class="post">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.
                      </p>

                      <p class="tag">
                        <i class="fa fa-tag"></i>
                        <a href="#">Good,</a>
                        <a href="#">Ui,</a>
                        <a href="#">Experience,</a>
                        <a href="#">Article</a>
                      </p>

                      <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                    </div> <!-- end .post-with-image -->

                    <div class="post-with-image">
                      <div class="date-month">
                        <a href="#">
                          <span class="date">12</span>
                          <span class="month">Sep</span>
                        </a>
                      </div>

                      <div class="post-image">
                        <img src="img/content/blog-list-image2.jpg" alt="">
                      </div>

                      <h2 class="title"><a href="#">This is Post Title</a></h2>

                      <p class="user">
                        <a href="#"><i class="fa fa-user"></i> Admin</a>
                        <a href="#"><i class="fa fa-folder-open-o"></i> Design</a>
                        <a href="#"><i class="fa fa-comments-o"></i> 2 Comments</a>
                      </p>

                      <p class="post">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.
                      </p>

                      <p class="tag">
                        <i class="fa fa-tag"></i>
                        <a href="#">Good,</a>
                        <a href="#">Ui,</a>
                        <a href="#">Experience,</a>
                        <a href="#">Article</a>
                      </p>

                      <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                    </div> <!-- end .post-with-image -->

                    <div class="post-without-image">
                      <div class="date-month">
                        <a href="#">
                          <span class="date">12</span>
                          <span class="month">Sep</span>
                        </a>
                      </div>

                      <h2 class="title"><a href="#">This is Post Title</a></h2>

                      <p class="user">
                        <a href="#"><i class="fa fa-user"></i> Admin</a>
                        <a href="#"><i class="fa fa-folder-open-o"></i> Design</a>
                        <a href="#"><i class="fa fa-comments-o"></i> 2 Comments</a>
                      </p>

                      <p class="post">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.
                      </p>

                      <p class="tag">
                        <i class="fa fa-tag"></i>
                        <a href="#">Good,</a>
                        <a href="#">Ui,</a>
                        <a href="#">Experience,</a>
                        <a href="#">Article</a>
                      </p>

                      <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                    </div> <!-- end .post-without-image -->


                    <div class="post-with-image">
                      <div class="date-month">
                        <a href="#">
                          <span class="date">12</span>
                          <span class="month">Sep</span>
                        </a>
                      </div>

                      <div class="post-image">
                        <img src="img/content/blog-list-image3.jpg" alt="">
                      </div>

                      <h2 class="title"><a href="#">This is Post Title</a></h2>

                      <p class="user">
                        <a href="#"><i class="fa fa-user"></i> Admin</a>
                        <a href="#"><i class="fa fa-folder-open-o"></i> Design</a>
                        <a href="#"><i class="fa fa-comments-o"></i> 2 Comments</a>
                      </p>

                      <p class="post">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.
                      </p>

                      <p class="tag">
                        <i class="fa fa-tag"></i>
                        <a href="#">Good,</a>
                        <a href="#">Ui,</a>
                        <a href="#">Experience,</a>
                        <a href="#">Article</a>
                      </p>

                      <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                    </div> <!-- end .post-with-image -->

                    <div class="post-without-image">
                      <div class="date-month">
                        <a href="#">
                          <span class="date">12</span>
                          <span class="month">Sep</span>
                        </a>
                      </div>

                      <h2 class="title"><a href="#">This is Post Title</a></h2>

                      <p class="user">
                        <a href="#"><i class="fa fa-user"></i> Admin</a>
                        <a href="#"><i class="fa fa-folder-open-o"></i> Design</a>
                        <a href="#"><i class="fa fa-comments-o"></i> 2 Comments</a>
                      </p>

                      <p class="post">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.
                      </p>

                      <p class="tag">
                        <i class="fa fa-tag"></i>
                        <a href="#">Good,</a>
                        <a href="#">Ui,</a>
                        <a href="#">Experience,</a>
                        <a href="#">Article</a>
                      </p>

                      <a class="post-read-more" href="#"><i class="fa fa-angle-right"></i>Read More</a>

                    </div> <!-- end .post-without-image -->

                  </div> <!-- end .blog-list -->

                  <div class="blog-list-pagination">

                    <ul class="pagination">
                      <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                    </ul>

                  </div> <!-- end .blog-list-pagination -->
                </div> <!-- end .company-blog -->
              </div> <!-- end .tab-pane -->

              <div class="tab-pane" id="company-contact">
                <div class="company-profile company-contact">

                  <h2>Contact Us</h2>

                  <div class="social-link text-right">
                    <ul class="list-inline">
                      <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                      <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                      <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                  </div>

                  <div class="company-text">
                    <p>
                      <strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue,
                        suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus.
                        Phasellus pharetra nulla ac diam. Quisque semper justo at risus.</strong>
                    </p>

                    <p>Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus,
                      sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet,
                      elit magna vulputate arcu, vel tempus metus leo non est.
                      Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque.
                      Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor,
                      et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit,
                      urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.
                    </p>
                  </div> <!-- end company-text -->


                  <div class="row">
                    <div class="col-md-6">

                      <div class="contact-map-company">
                        <div id="contact_map_canvas_one">

                        </div>
                      </div> <!-- end .map-section -->

                      <h3>Headquarters</h3>

                      <h5>Address Details</h5>

                      <div class="address-details clearfix">
                        <i class="fa fa-map-marker"></i>

                        <p>
                          <span>1234 Hyde Street</span>
                          <span>San Francisco</span>
                          <span>CA 94043</span>
                        </p>
                      </div>

                      <div class="address-details clearfix">
                        <i class="fa fa-phone"></i>

                        <p>
                          <span><strong>Phone:</strong> +1 123-456-7890</span>
                          <span><strong>Fax:</strong> +1 123-456-7891</span>
                        </p>
                      </div>

                      <div class="address-details clearfix">
                        <i class="fa fa-envelope-o"></i>

                        <p>
                          <span><strong>E-mail:</strong> example@example.com</span>
                          <span><span><strong>Website:</strong> www.example.com</span></span>
                        </p>
                      </div>

                      <h5>Openig Hours</h5>

                      <div class="address-details clearfix">
                        <i class="fa fa-clock-o"></i>

                        <p>
                          <span><strong>Mo-Fri:</strong> 9AM - 5PM</span>
                          <span><span><strong>Saturday:</strong> 10AM - 2PM</span></span>
                          <span><strong>Sunday:</strong> Closed</span>
                        </p>
                      </div>

                    </div> <!-- end main grid layout -->

                    <div class="col-md-6">
                      <div class="contact-map-company">
                        <div id="contact_map_canvas_two">

                        </div>
                      </div> <!-- end .map-section -->

                      <h3>Retailing Point</h3>

                      <h5>Address Details</h5>

                      <div class="address-details clearfix">
                        <i class="fa fa-map-marker"></i>

                        <p>
                          <span>1234 Hyde Street</span>
                          <span>San Francisco</span>
                          <span>CA 94043</span>
                        </p>
                      </div>

                      <div class="address-details clearfix">
                        <i class="fa fa-phone"></i>

                        <p>
                          <span><strong>Phone:</strong> +1 123-456-7890</span>
                          <span><strong>Fax:</strong> +1 123-456-7891</span>
                        </p>
                      </div>

                      <div class="address-details clearfix">
                        <i class="fa fa-envelope-o"></i>

                        <p>
                          <span><strong>E-mail:</strong> example@example.com</span>
                          <span><span><strong>Website:</strong> www.example.com</span></span>
                        </p>
                      </div>

                      <h5>Openig Hours</h5>

                      <div class="address-details clearfix">
                        <i class="fa fa-clock-o"></i>

                        <p>
                          <span><strong>Mo-Fri:</strong> 9AM - 5PM</span>
                          <span><span><strong>Saturday:</strong> 10AM - 2PM</span></span>
                          <span><strong>Sunday:</strong> Closed</span>
                        </p>
                      </div>

                    </div> <!-- end main grid layout -->
                  </div> <!-- end .row -->

                  <h3>Send Us A Message</h3>
                  <form class="comment-form">
                    <div class="row">
                      <div class="col-md-4">
                        <input type="text" placeholder="Name *" required>
                      </div>

                      <div class="col-md-4">
                        <input type="email" placeholder="Email *" required>
                      </div>

                      <div class="col-md-4">
                        <input type="url" placeholder="Website">
                      </div>
                    </div>

                    <textarea placeholder="Your Comment ..." required></textarea>

                    <button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i> Send Message</button>
                  </form>

                </div> <!-- end .company-contact -->
              </div> <!-- end .tab-pane -->
            </div> <!-- end .tab-content -->

          </div> <!-- end .page-content -->

        </div> <!-- end .main-grid layout -->

        <div class="col-md-3 col-md-pull-9 category-toggle">
          <button><i class="fa fa-briefcase"></i></button>
          <div class="page-sidebar company-sidebar">

            <ul class="company-category nav nav-tabs home-tab" role="tablist">
              <li class="active">
                <a href="#company-profile" role="tab" data-toggle="tab"><i class="fa fa-newspaper-o"></i><?php echo __('Профиль'); ?></a>
              </li>
            </ul>

<!--
            <div class="own-company">
              <a href="#"><i class="fa fa-question-circle"></i>Own This Company</a>
            </div>
-->

            <div class="contact-details">
              <h2><?php echo __('Contact Details'); ?></h2>
              
               <table class="table" style="width: 100%">
		            <tr>
			            <td><strong><?php echo __('Название'); ?></strong></td>
			            <td class="col-right"><?php echo h($company['Company']['name']); ?></td>
		            </tr>
		            <tr>
			            <td><strong><?php echo __('Основан'); ?></strong></td>
			            <td class="col-right"><?php echo h($company['Company']['date_found']); ?></td>
		            </tr>
		            <tr>
			            <td><strong><?php echo __('Контакты'); ?></strong></td>
			            <td class="col-right"><?php echo h($company['Company']['contacts']); ?></td>
		            </tr>
		            <tr>
			            <td><strong><?php echo __('Адрес'); ?></strong></td>
			            <td class="col-right"><?php echo h($company['Company']['address']); ?></td>
		            </tr>
	            </table>
            </div>

          </div> <!-- end .page-sidebar -->
        </div> <!-- end .main-grid layout -->
      </div> <!-- end .row -->

    </div> <!-- end .container -->

  </div> <!-- end #page-content -->