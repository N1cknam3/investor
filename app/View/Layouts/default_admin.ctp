<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <?php
	echo $this->Html->meta('icon', 'img/favicon.png');
	echo $this->Html->css('http://fonts.googleapis.com/css?family=Raleway:400,700,600,800%7COpen+Sans:400italic,400,600,700');
	echo $this->Html->css('custom-style');
	echo $this->Html->css('style');
	echo $this->Html->css('animate');
		
	echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
	echo $this->Html->script('https://code.jquery.com/ui/1.11.1/jquery-ui.js');
	echo $this->Html->script('globo/jquery.ba-outside-events.min.js');
	echo $this->Html->script('https://maps.google.com/maps/api/js?sensor=true');

	echo $this->Html->script('nickname/helper-functions');
	echo $this->Html->script('nickname/functions');
	echo $this->Html->script('nickname/table-container');
  ?>

  <!--[if IE 9]>
    <script src="js/media.match.min.js"></script>
  <![endif]-->
	
	
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	
	<?php /* Строка адреса для использования в AJAX-запросах выдачи текущего адреса по $.url(urlPath) */?>
	<base href="<?php echo Router::url('/'); ?>" />
	
</head>
<body>
	<div id="main-wrapper">
		
		<?php echo $this->element('main_header_without_slider_admin'); ?>
		
		<?php echo $this->fetch('content'); ?>
		
		<?php echo $this->element('main_footer'); ?>
		
	</div>
	<?php //echo $this->element('sql_dump'); ?>
	
	<?php
		
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
		echo $this->Html->script('https://code.jquery.com/ui/1.11.1/jquery-ui.js');
		echo $this->Html->script('globo/jquery.ba-outside-events.min.js');
		echo $this->Html->script('https://maps.google.com/maps/api/js?sensor=true');
		
		echo $this->Html->script('globo/gomap.js');
		echo $this->Html->script('globo/gmaps.js');
		echo $this->Html->script('globo/bootstrap.min.js');
		echo $this->Html->script('globo/owl.carousel.js');
		echo $this->Html->script('globo/scripts.js');
	?>
	
</body>
</html>
