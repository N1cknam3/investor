<?php echo $this->Flash->render(); ?>

<div id="page-content">
    <div class="container">
      <div class="page-content">
        <div class="contact-us">
		  <div class="row">

			<div class="col-md-12">
				<div class="contact-form">
					<?php echo $this->Form->create('Subcategory'); ?>

						<h3><?php echo __('Новая подкатегория'); ?></h3>
					<?php
						echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Название')));
						echo $this->Form->input('category_id', array('label' => false, 'data-placeholder' => $category_id, 'data-selected' => $category_id, 'options' => $categories, 'selected' => $category_id));
					?>
					
					<?php echo $this->Form->end(__('Готово')); ?>
				</div>
			</div>

          </div> <!-- end .row -->
        </div> <!-- end .contact-us -->

		<div class="actions blog-list">
			<h3><?php echo __('Действия'); ?></h3>
			<?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Список категорий'), array('controller' => 'categories', 'action' => 'index'), array('class' => 'post-read-more', 'escape' => false)); ?>
		</div>

      </div> <!-- end .page-content -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->