<header id="header">
  <div class="header-top-bar">
    <div class="container">
      <!-- HEADER-LOGIN -->
      <div class="header-login">

         <?php echo $this->Form->postLink('<i class="fa fa-power-off"></i> '.__('Выход'), array('controller' => 'admin', 'action' => 'logout'), array('confirm' => __('Вы точно хотите выйти из системы?'), 'escape' => false)); ?>
         <?php echo $this->Html->link('<i class="fa fa-home"></i> '.__('Кабинет'), array('controller' => 'admin', 'action' => 'index'), array('escape' => false)); ?>

      </div> <!-- END .HEADER-LOGIN -->

      <!-- HEADER-LOG0 -->
      <div class="header-logo text-center">
        <?php
          echo $this->Html->image('investor_logo.png', array('style' => 'height: 30px;'));
        ?>
      </div>
      <!-- END HEADER LOGO -->

      <?php echo $this->element('header_language_select'); ?>

    </div><!-- END .CONTAINER -->
  </div>
  <!-- END .HEADER-TOP-BAR -->

  <!-- HEADER SEARCH SECTION -->
  <div class="header-search slider-home">
    <div class="header-search-bar">
      <?php echo $this->Form->create('Search', array('url' => array('controller' => 'main', 'action' => 'search_result'))); ?>

        <div class="container">
          <button class="toggle-btn" type="submit"><i class="fa fa-bars"></i></button>

<!--             <div class="search-value"> -->
            
            <div class="category-search">
              <?php echo $this->Form->input('category_id', array('label' => false, 'data-placeholder' => __('Категория'), 'options' => @$categories_global, 'div' => false)); ?>
            </div>
            
            <div class="keywords">
              <?php echo $this->Form->input('keywords', array('label' => false, 'placeholder' => __('Поиск'), 'div' => false)); ?>
            </div>

            <?php echo $this->Form->button('<i class="fa fa-search"></i>', array('type' => 'submit', 'class' => 'search-btn', 'escape' => false)); ?>
<!--             </div> -->
        </div> <!-- END .CONTAINER -->
      <?php echo $this->Form->end(); ?>
    </div> <!-- END .header-search-bar -->

    <div class="slider-content">

      <div id="home-slider" class="owl-carousel owl-theme">

        <div class="item"><?php echo $this->Html->image('back/back_img_one.jpg', array('alt' => 'CakePHP', 'style' => 'width: auto;')); ?>
          <div class="slide-content">
            

          </div>


        </div>

        <div class="item"><?php echo $this->Html->image('back/back_img_two.jpg', array('alt' => 'CakePHP', 'style' => 'width: auto;')); ?>
          <div class="slide-content">
            

          </div>

        </div>

        <div class="item"><?php echo $this->Html->image('back/back_img_three.png', array('alt' => 'CakePHP', 'style' => 'width: auto;')); ?>
          <div class="slide-content">
            

          </div>
        </div>
      </div>

      <div class="customNavigation">
        <a class="btn prev"><i class="fa fa-angle-left"></i></a>
        <a class="btn next"><i class="fa fa-angle-right"></i></a>
      </div>

    </div> <!-- END .slider-content -->
  </div> <!-- END .SEARCH and slide-section -->

  <?php
// 	    echo $this->element('main_menubar');
  ?>
</header> <!-- end #header -->