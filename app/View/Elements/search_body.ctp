<div id="page-content">
    <div class="container">
      <div class="row">

        <div class="col-md-9 col-md-push-3">
          <div class="page-content">

            <div class="product-details-list view-switch">
              <div class="tab-content">

                <div class="tab-pane active">
	                
	              <?php
		              
		              $category_name = __('Поиск');
		              	  
		              if (isset($choosed_category)) {
			              $category_name = '<a href="'
		              	      .Router::url(array('controller' => 'main', 'action' => 'search'))
		              	      .'">'.$category_name.'</a>'
		              	      .'<span class="category-name"><b>\</b></span><span class="category-name-1">';
					  		
					  	  if (isset($choosed_subcategory)) {
						  	  $category_name .= '<a href="'
			          			  .Router::url(array('controller' => 'main', 'action' => 'search/all/'.$choosed_category['Category']['id']))
					  			  .'">'.$choosed_category['Category']['name'].'</a>';
					  	  } else {
						  	  $category_name .= $choosed_category['Category']['name'];
					  	  }
					  	  
					  	  $category_name .= '</span>';
		              }
		              
		              $subcategory_name = null;
		              if (isset($choosed_subcategory)) {
			          	$subcategory_name = '<span class="category-name"><b>\</b>'.$choosed_subcategory['Subcategory']['name'].'</span>';
		              }
			          
			          $comments = '<span class="comments">99</span>';
			          
			          echo '<h2>'.$category_name.$subcategory_name.'</h2>';
	              ?>
	                
                  <!-- <h2>E-commerce<span class="category-name"><b>\</b>Books</span><span class="comments">99</span></h2> -->

                  <div class="change-view">

                    <button class="grid-view"><i class="fa fa-th"></i></button>
                    <button class="list-view active"><i class="fa fa-bars"></i></button>

                    <div class="sort-by">

                    	<?php echo $this->element('paginator_sorting'); ?>

                    </div>

                    <?php echo $this->element('paginator'); ?>

                  </div> <!-- end .change-view -->

                  <div class="row clearfix">
	                  
	                <?php
		                
		                if (isset($result_data) && !empty($result_data)) {
		                
			                $class_ad = (isset($set_advertisement) && $set_advertisement) ? 'with-ad' : null;
			                
			                foreach($result_data as $data) {
					                
					                $company_url = Router::url(array('controller' => 'companies', 'action' => 'view/'.$data['Company']['id']));
					                
					                echo '<div class="col-sm-4 col-xs-6 '.$class_ad.'"><div class="single-product">';
				                
						                echo '<figure>';
	// 					                	echo '<img src="'.$company['logo'].'" alt="">';
						                	
						                	echo '<div style="width: 100%; height: 100%; background-image: url('.$data['Company']['logo'].'); background-size: contain; background-repeat: no-repeat; background-position: 50% 50%;"></div>';
						                	
						                	echo '<figcaption>';
						                	
						                		echo '<div class="read-more"><a href="'.$company_url.'"><i class="fa fa-angle-right"></i> '.__('Подробнее').'</a></div>';
						                			
						                	echo '</figcaption>';
						                echo '</figure>';
						                
						                echo '<h4><a href="'.$company_url.'">'.$data['Company']['name'].'</a></h4>';
						                
						                //	TODO: может быть несколько категорий!
						                echo '<h5><a href="'
						                	.Router::url(array('controller' => 'main', 'action' => 'search/all/'.$data['Category']['id'])).'">'.$data['Category']['name'].'</a>, <a href="'
						                	.Router::url(array('controller' => 'main', 'action' => 'search/all/'.$data['Category']['id'].'/'.$data['Subcategory']['id'])).'">'.$data['Subcategory']['name'].'</a></h5>';
						                
						                echo '<p>'.$data['Company']['short_desc'].'</p>';
	
							  			echo '<a class="read-more" href="'.$company_url.'"><i class="fa fa-angle-right"></i>'.__('Подробнее').'</a>';
					                
					                echo '</div></div>';
				                
			                }
		                } else {
		                	echo $this->element('placeholder_search', array('placeholder_text' => __('К сожалению, нет результатов по вашему запросу')));
		                }
	                ?>
                    
                    <?php
	                    if (isset($set_advertisement) && $set_advertisement) {
		                    echo '<div class="advertisement"><p>'.__('Реклама').'</p><img src="img/add.jpg" alt=""></div>';
	                    }
                    ?>

                    <div class="pagination-center">
                      <?php echo $this->element('paginator'); ?>
                    </div>

                  </div> <!-- end .row -->
                </div> <!-- end .tabe-pane -->
                
              </div> <!-- end .tabe-content -->

              <!--
<div class="advertisement">
                <p>Advertisement</p>
                <img src="img/add.jpg" alt="">
              </div>
-->

            </div> <!-- end .product-details -->
          </div> <!-- end .page-content -->
        </div>

        <div class="col-md-3 col-md-pull-9 category-toggle">
          <button><i class="fa fa-list"></i></button>

          <div class="page-sidebar">

            <!-- Category accordion -->
            <div id="categories" class="no-margin-top">
              <div class="accordion">
                <ul class="nav nav-tabs accordion-tab" role="tablist">

					<?php if (isset($search_result)): ?>
						<li class="active">
							<a href="#"><?php echo __('Результат поиска'); ?></a>
						</li>
					<?php endif; ?>

					<?php

						foreach($categories as $category) {
							
							$class_category = (@$choosed_category['Category']['id'] == $category['Category']['id']) ? 'active' : null;
							
							echo '<li class="'.$class_category.'">';
								
								if (count($category['Subcategory'])) {
									echo '<a href="#">'.$category['Category']['name'].'</a>';
									
									echo '<div>';
										
										$class_subcategory_all = ($class_category && !isset($choosed_subcategory)) ? 'active' : null;
										
										echo '<a class="'.$class_subcategory_all.'" href="'
											.Router::url(array('controller' => 'main', 'action' => 'search/all/'.$category['Category']['id']))
											.'">'.__('Показать все').'</a>';
										
										foreach($category['Subcategory'] as $subcategory) {
											$class_subcategory = (@$choosed_subcategory['Subcategory']['id'] == $subcategory['id']) ? 'active' : null;
											echo '<a class="'.$class_subcategory.'" href="'
												.Router::url(array('controller' => 'main', 'action' => 'search/all/'.$category['Category']['id']
												.'/'.$subcategory['id']))
												.'">'.$subcategory['name'].'</a>';
										}
										
									echo '</div>';
								} else {
									echo '<a href="'.Router::url(array('controller' => 'main', 'action' => 'search/all/'.$category['Category']['id'])).'">'.$category['Category']['name'].'</a>';
								}
								
							unset($class_category);
							
							echo '</li>';
							
						}
					?>
                </ul>
              </div> <!-- end .accordion -->
            </div> <!-- end #categories -->


          </div> <!-- end .page-sidebar -->
        </div> <!-- end grid layout-->
      </div> <!-- end .row -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
