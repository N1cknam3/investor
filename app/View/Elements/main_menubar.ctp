<div class="container">
  <div class="header-nav-bar home-slide">
    <nav>

      <button><i class="fa fa-bars"></i></button>

      <ul class="primary-nav list-unstyled">
        <li class="bg-color"><a href="#">Home<i class="fa fa-angle-down"></i></a>
          <ul>
            <li><a href="home-map-grid.html">Home-map(grid)</a></li>
            <li><a href="home-map-list.html">Home-map(list)</a></li>
            <li><a href="index.html">Home slider(view-1)</a></li>
            <li><a href="home-category.html">Home slider(view-2)</a></li>
          </ul>

        </li>

        <li class=""><a href="#">Company<i class="fa fa-angle-down"></i></a>

          <ul>
            <li><a href="company-profile.html">Company(Profile)</a></li>
            <li><a href="company-product.html">Company(Product)</a></li>
            <li><a href="company-portfolio.html">Company(Portfolio)</a></li>
            <li><a href="company-events.html">Company(Events)</a></li>
            <li><a href="company-blog.html">Company(Blog)</a></li>
            <li><a href="company-contact.html">Company(contact)</a></li>
          </ul>

        </li>

        <li><a href="sample-page.html">Sample Page</a></li>
        <li><a href="shortcodes.html">Shortcodes</a></li>

        <li class="">
          <a href="#">Blog<i class="fa fa-angle-down"></i></a>

          <ul>
            <li><a href="blog-list.html">Blog list</a></li>
            <li><a href="blog-post.html">Blog-post</a></li>
          </ul>

        </li>

        <li><a href="price-listing.html">Price Listing</a></li>
        <li><a href="about-us.html">About Us</a></li>
        <li><a href="contact-us.html">Contact Us</a></li>
      </ul>
    </nav>
  </div> <!-- end .header-nav-bar -->
</div> <!-- end .container -->