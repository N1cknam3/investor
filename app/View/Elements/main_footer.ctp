  <footer id="footer">
    <div class="main-footer">

      <div class="container">
        <div class="row sponsor-container">

          <div class="col-sm-3 sponsor">
            <a class="footer-logo-1" href="http://kursksu.ru" target="_blank">
              <img src="http://kursksu.ru/images/logoH.png">
            </a>
          </div>
          <div class="col-sm-3 sponsor">
            <a class="footer-logo-2" href="https://www.b-tu.de" target="_blank">
              <img src="https://www.b-tu.de/fileadmin/btu-cs/images/logo/BTULogo_englisch_weiss.png">
            </a>
          </div>
          <div class="col-sm-3 sponsor">
            <a class="footer-logo-3" href="https://kursk.tpprf.ru/ru/" target="_blank">
              <?php echo $this->Html->image('ktpp.png'); ?>
            </a>
          </div>
          <div class="col-sm-3 sponsor">
            <a class="footer-logo-4" href="https://www.cottbus.ihk.de/" target="_blank">
              <?php echo $this->Html->image('cottbus-tpp.png'); ?>
            </a>
          </div>
          
        </div> <!-- end .row -->
      </div> <!-- end .container -->
    </div> <!-- end .main-footer -->

    <div class="copyright">
      <div class="container">
        <?php
          echo sprintf(__('Copyright %s &copy; Investments time. All rights reserved. Powered by  %s'), date('Y'), '<a href="http://itdevelop.su/">IT-DEVELOP</a>');
        ?>
      </div> <!-- END .container -->
    </div> <!-- end .copyright-->
  </footer> <!-- end #footer -->