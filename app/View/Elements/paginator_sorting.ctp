<?php
	$sorting_placeholder = "-".__("упорядочить");
	$paginator_options = $this->Paginator->params()['options'];
	if (!empty($paginator_options) && isset($paginator_options['sort'])) {
		switch ($paginator_options['sort']) {
			case 'name':
				$sorting_placeholder = __('Название');
				break;
			case 'priority':
				$sorting_placeholder = __('Интересное');
				break;
			case 'created_at':
				$sorting_placeholder = __('Добавлено');
				break;
		}
		if (isset($paginator_options['direction'])) {
				switch ($paginator_options['direction']) {
				case 'asc':
					$sorting_placeholder .= ' &#8681;';
					break;
				case 'desc':
					$sorting_placeholder .= ' &#8679;';
					break;
			}
		}
	}
?>

<script type="text/javascript">
     function gotoHref(string_link) {
     	var link = new DOMParser().parseFromString(string_link, "text/xml").firstChild;
     	var url = $(link).attr('href');
     	if (url)
     		window.location = url;
     };
</script>

<select class="sorting" data-placeholder=<?php echo "'$sorting_placeholder'" ?> onchange="gotoHref($(this).find(':selected').attr('href'))">
	<option value="option1" href=<?php echo '\''.$this->Paginator->sort('name').'\'' ?>><?php echo __('Название') ?></option>
	<option value="option2" href=<?php echo '\''.$this->Paginator->sort('priority').'\'' ?>><?php echo __('Интересное') ?></option>
	<option value="option3" href=<?php echo '\''.$this->Paginator->sort('created_at').'\'' ?>><?php echo __('Добавлено') ?></option>
</select>