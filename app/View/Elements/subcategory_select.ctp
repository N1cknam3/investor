<?php echo $this->Form->input('Subcategory.Subcategory', array(
	'label' => __('Подкатегория'),
	'type' => 'select', 
	'options' => $subcategories,
	'selected' => @$subcategory_id,
	'data-selected' => @$subcategory_id,
	'div' => 'col-md-6',
	'class' => $added ? 'added' : null
)); ?>