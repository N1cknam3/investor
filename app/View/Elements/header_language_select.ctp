<!-- HEADER-LANGUAGE -->
<div class="header-language">

  <a href="#">
    <span>

      <?php
        $lang = $this->Session->check('Config.language') ? $this->Session->read('Config.language') : Configure::read('Config.language');

        if (in_array($lang, array('deu', 'de')))
          echo 'DE';
        elseif (in_array($lang, array('rus', 'ru')))
          echo 'РУ';
        else
          echo 'РУ';
      ?>

    </span>
    <i class="fa fa-chevron-down"></i>
  </a>

  <ul class="list-unstyled">
    <li class="active"><?php echo $this->Html->link('РУ',
      array('action' => 'rus')
    ); ?></li>
    <li class="active"><?php echo $this->Html->link('DE',
      array('action' => 'deu')
    ); ?></li>
  </ul>
</div> <!-- END HEADER-LANGUAGE -->