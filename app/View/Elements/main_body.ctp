<div id="content">

<?php echo $this->Flash->render(); ?>

<div id="page-content" class="home-slider-content">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

		  <div class="col-md-12">
            <div class="page-content">

              <div class="product-details">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">
	                  
                    <h3><?php echo $this->Html->link(__('Интересные <span>компании</span>'), array('action' => 'search/new'), array('escape' => false)); ?></h3>
                    <div class="row clearfix">
                      
                      <?php
	                      if (count($companies) > 0) {
		                      
		                      for($i = 0; ($i < 8) && ($i < count($companies)); $i++) {
			                    echo $this->MainWidgets->NewCompanyCard(
			                    	$companies[$i]['name'], $companies[$i]['address'], Router::url(array('controller' => 'companies', 'action' => 'view/'.$companies[$i]['id'])));
		                      }
		                      
		                      if (count($companies) > 8)
		                      	echo $this->MainWidgets->ShowMoreButton();
		                      
	                      } else {
                          echo '<div class="col-md-12" style="text-align: center;"><h4 style="color: lightgray;">'.__('К сожалению, на данный момент ничего нет').'</h4></div>';
                        }

                      ?>

                    </div> <!-- end .row -->
                    
                    <hr style=" height: 5px; display: block; border-style: dashed; border-width: 1px 0px; margin: 30px 0; ">
                    
                    <h3><?php echo $this->Html->link(__('Список <span>по категориям</span>'), array('action' => 'search/all'), array('escape' => false)); ?></h3>
                    <div class="row clearfix">
	                    
	                    <?php
		                    echo $this->MainWidgets->CategoryList($categories, Router::url(array('controller' => 'main', 'action' => 'search/all')));
	                    ?>
	                    
                    </div> <!-- end .row -->
                    
                    <!-- <hr style=" height: 5px; display: block; border-style: dashed; border-width: 1px 0px; margin: 30px 0; ">
                    
                    <h3>Информация <span>к ознакомлению</span></h3>
                    <div class="row clearfix">
	                    
	                    <div class="col-sm-12">
				            <h6>1/4 Text-column</h6>
				            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
				              Maiores explicabo numquam accusamus voluptatibus odit.</p> -->
				        </div>
	                    
                    </div>
                    
                  </div> <!-- end .tabe-pane -->
                  
                </div> <!-- end .tabe-content -->

              </div> <!-- end .product-details -->
            </div> <!-- end .page-content -->
          </div>
          
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
</div>
                