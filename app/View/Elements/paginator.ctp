<ul class="pagination">
<?php
	echo $this->Paginator->prev('<', array('tag' => 'li', 'disabledTag' => 'a'), null, array('class' => 'prev disabled', 'escape' => false));
	echo $this->Paginator->numbers(array('separator' => ' ', 'tag' => 'li', 'currentClass' => 'active', 'currentTag' => 'a'));
	echo $this->Paginator->next('>', array('tag' => 'li', 'disabledTag' => 'a'), null, array('class' => 'next disabled', 'escape' => false));
	for($i=25;$i<=100;$i+=$i) {
		$active = $i == $this->Paginator->params()['limit'] ? 'active' : null;
		echo '<li class="'.$active.'">'.$this->Paginator->link($i, array('limit' => $i)).'</li>';
	}
?>
</ul>