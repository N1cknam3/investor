<?php
class MainWidgetsHelper extends AppHelper {
	
	public $helpers = array('Html', 'Form', 'Js');
	
	public function NewCompanyCard ($name, $address, $link = "#"){
		
		$answer = '';
				
		$answer .= '<div class="col-md-3 col-sm-4 col-xs-6">';
		    $answer .= '<div class="category-item" style=" display: table; ">';
		        $answer .= '<a href="'.$link.'" style=" display: table-cell; padding: 0 7px; vertical-align: middle; text-transform: none; ">';
		        	$answer .= '<p>'.$name.'</p>';
					$answer .= '<p style=" margin-bottom: 0; font-size: small; font-weight: normal; ">'.$address.'</p>';
				$answer .= '</a>';
		    $answer .= '</div>';
		$answer .= '</div>';
		
		return $answer;
	}
	
	public function ShowMoreButton() {
	
		$answer = '';	
	
		$answer .= '<div class="view-more">';
        	$answer .= '<a class="btn btn-default text-center" href="#"><i class="fa fa-plus-square-o"></i>Показать больше</a>';
        $answer .= '</div>';
        
        return $answer;
	}
	
	public function CategoryList($dataList, $linkBase, $colCount = 3) {
		
		$answer = '';
		
			foreach($dataList as $key => $value) {
				
	            $answer .= '<a href="'.$linkBase."/".$key.'">';
	                $answer .= '<div class="col-sm-4">';
			            $answer .= '<p>'.$value.'</p>';
					$answer .= '</div>';
	            $answer .= '</a>';
			}
        
        return $answer;
	}
	
}
?>