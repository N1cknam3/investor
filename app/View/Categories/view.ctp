<div id="content">

<?php echo $this->Flash->render(); ?>

<div id="page-content" class="home-slider-content cabinet">
    <div class="container">
      <div class="home-with-slide">
        <div class="row">

		  <div class="col-md-12">
            <div class="page-content">

              <div class="product-details-list my-list">
                <div class="tab-content">

                  <div class="tab-pane active" id="all-categories">

                  	<!-- Content goes here -->

                  	<h2><?php echo h($category['Category']['name']); ?></h2>

                  	<h3><?php echo __('Список подкатегорий'); ?></h3>
					<div class="row clearfix">
						<?php
							if (empty($subcategories))
								echo $this->element('placeholder_search', array('placeholder_text' => __('У данной категории нет подкатегорий')));

	                  		foreach ($subcategories as $subcategory):
	                  	?>

								<div class="col-sm-12 col-xs-12"><div class="single-product">
					                
					                <h4><?php echo h($subcategory['Subcategory']['name']); ?></h4>

									<?php
					                	foreach($languages as $language) {
					                		echo $this->Html->link(' <i class="fa fa-pencil-square-o"></i>'.__('Изменить')." $language", array('controller' => 'subcategories', 'action' => 'edit', $subcategory['Subcategory']['id'], $language), array('class' => 'read-more', 'escape' => false ));
					                	}
					                ?>

									<?php echo $this->Form->postLink('<i class="fa fa-trash-o"></i>'.__('Удалить'), array('controller' => 'subcategories', 'action' => 'delete', $subcategory['Subcategory']['id']), array('confirm' => __('Вы действительно желаете удалить "%s"?', $subcategory['Subcategory']['name']), 'class' => 'read-more', 'escape' => false)); ?>
				                
				                </div></div>
				        <?php endforeach; ?>
			        </div>

					<div class="row">
						<?php echo $this->element('paginator'); ?>
					</div>

					<div class="actions blog-list">
						<h3><?php echo __('Действия'); ?></h3>
						<?php echo $this->Html->link('<i class="fa fa-pencil"></i>'.__('Изменить категорию'), array('action' => 'edit', $category['Category']['id']), array('class' => 'post-read-more', 'escape' => false)); ?>
						<?php echo $this->Form->postLink('<i class="fa fa-trash-o"></i>'.__('Удалить категорию'), array('action' => 'delete', $category['Category']['id']), array('confirm' => __('Вы действительно желаете удалить "%s"?', $category['Category']['name']), 'class' => 'post-read-more', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Список категорий'), array('action' => 'index'), array('class' => 'post-read-more', 'escape' => false)); ?>
						<?php echo $this->Html->link('<i class="fa fa-plus"></i>'.__('Новая подкатегория'), array('controller' => 'subcategories', 'action' => 'add/'.$category['Category']['id']), array('class' => 'post-read-more', 'escape' => false)); ?>
					</div>

					<!-- End content -->

					</div> <!-- end .tabe-pane -->
                  
                </div> <!-- end .tabe-content -->

              </div> <!-- end .product-details -->
            </div> <!-- end .page-content -->
          </div>
          
        </div> <!-- end .row -->
      </div> <!-- end .home-with-slide -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->
</div>
