<?php echo $this->Flash->render(); ?>

<div id="page-content">
    <div class="container">
      <div class="page-content">
        <div class="contact-us">
		  <div class="row">

			<div class="col-md-12">
				<div class="contact-form">
					<?php echo $this->Form->create('Category'); ?>

						<h3><?php echo __('Редактирование категории'); ?></h3>
					<?php
						echo $this->Form->input('id');
						echo $this->Form->input('name', array('label' => false, 'placeholder' => __('Название')));
					?>
					
					<?php echo $this->Form->end(__('Готово')); ?>
				</div>
			</div>

          </div> <!-- end .row -->
        </div> <!-- end .contact-us -->

		<div class="actions blog-list">
			<h3><?php echo __('Действия'); ?></h3>
			<?php echo $this->Form->postLink('<i class="fa fa-trash-o"></i>'.__('Удалить'), array('action' => 'delete', $this->Form->value('Category.id')), array('confirm' => __('Вы действительно желаете удалить "%s"?', $this->Form->value('Category.name')), 'class' => 'post-read-more', 'escape' => false)); ?>
			<?php echo $this->Html->link('<i class="fa fa-bars"></i>'.__('Список категорий'), array('action' => 'index'), array('class' => 'post-read-more', 'escape' => false)); ?>
		</div>

      </div> <!-- end .page-content -->
    </div> <!-- end .container -->
  </div>  <!-- end #page-content -->