<?php

App::uses('AppController', 'Controller');

class AdminController extends AppController {

	public $uses = array();
	public $helpers = array('Html', 'MainWidgets');
	
	public function beforeFilter() {
		parent::beforeFilter();
		
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->logout();
			}
			$this->layout = "default_admin";
		}
	}

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
		//$this->layout = "default_admin";
		$title_for_layout = __("Главная");
		$companies = $this->getElementsFromModel("all", "Company", array());
		$companies = $this->redunantModelName($companies, "Company");
		$categories = $this->getElementsFromModel("list", "Categories", array());
		$this->set(compact('title_for_layout', 'companies', 'categories'));
	}
	
	public function search($category_id = null, $subcategory_id = null) {
		
		$set_advertisement = true;
		$this->set(compact($set_advertisement, 'set_advertisement'));
		
		//	Список всех категорий для бокового меню поиска
		$categories = $this->getElementsFromModel("all", "Category", array());
		$this->set(compact('categories'));
		
		$this->set(compact('category_id', 'subcategory_id'));
		
		if ($category_id) {
			$result_data = $this->getElementsFromModel("all", "Subcategory", array('Subcategory.category_id' => $category_id));
			$this->set(compact('result_data'));
			
			$choosed_category = $this->getElementsFromModel("first", "Category", array('Category.id' => $category_id));
			$this->set(compact('choosed_category'));
		}
		
		if ($subcategory_id) {
			$result_data = $this->getElementsFromModel("all", "Subcategory", array('Subcategory.id' => $subcategory_id));
			$this->set(compact('result_data'));
			
			$choosed_subcategory = $this->getElementsFromModel("first", "Subcategory", array('Subcategory.id' => $subcategory_id));
			$this->set(compact('choosed_subcategory'));
		}
		
	}
}
