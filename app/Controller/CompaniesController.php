<?php
App::uses('AppController', 'Controller');
/**
 * Companies Controller
 *
 * @property Company $Company
 * @property PaginatorComponent $Paginator
 */
class CompaniesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $paginate = array(
        'limit' => 25
    );

	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->Paginator->settings = $this->paginate;
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->layout = "default";
			} else 
				$this->layout = "default_admin";
		}

		$this->Company->locale = $this->_getLanguage();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		if (!$this->is_login())
			$this->logout();

		$this->set('languages', $this->languages);
		$this->Company->recursive = 1;
		$this->set('companies', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Не найдена запрашиваемая компания'));
		}
		$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
		$company = $this->Company->find('first', $options);
		if (isset($company['Subcategory']) && isset($company['Subcategory'][0])) {
			$this->loadModel('Subcategory');
			$subcategory = $this->Subcategory->find('first', array('conditions' => array('Subcategory.id' => $company['Subcategory'][0]['id'])));
			$company['Subcategory'][0] = $subcategory['Subcategory'];
		}
		$this->set('company', $company);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if (!$this->is_login())
			$this->logout();

		// Выбираем список категорий и подкатегорий для вывода выпадающих списков
		// (категории - для первоначального отображения, нужные подкатегории подгрузятся AJAX-ом при выборе соответствующей категории пользователем)
		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();
		$categories = $this->Category->find('list', array('order' => array('name' => 'ASC')));
		reset($categories); // сброс итерации массива для выбора ПЕРВОЙ категории при поиске ее подкатегорий

		$this->loadModel('Subcategory');
		$this->Subcategory->recursive = 1;
		$this->Subcategory->locale = $this->_getLanguage();
		$subcategories = $this->Subcategory->find('list', array('conditions' => array('category_id' => key($categories)), 'order' => array('name' => 'ASC'))); // поиск подкатегорий первой для категории в списке

		$this->set(compact('categories', 'subcategories'));

		if ($this->request->is('post')) {

			$this->request->data['Company']['created_at'] = date("Y-m-d H:i:s");
			$this->request->data['Company']['updated_at'] = date("Y-m-d H:i:s");

			$this->Company->create();

			$success = true;
			foreach ($this->languages as $lang) {
				$this->Company->setLocale($lang);
				$success = $success && $this->Company->save($this->request->data);
			}
			if ($success)
				return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $lang = null) {
		if (!$this->is_login())
			$this->logout();

		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Не найдена запрашиваемая компания'));
		}

		$this->Company->setLocale($lang);

		if ($this->request->is(array('post', 'put'))) {

			$this->request->data['Company']['updated_at'] = date("Y-m-d H:i:s");

			if ($this->Company->save($this->request->data)) {
				// $this->Flash->success(__('Компания была успешно обновлена.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Flash->error(__('Не удалось обновить компанию. Пожалуйста, попробуйсте еще раз.'));
			}
		} else {
			$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
			$this->request->data = $this->Company->find('first', $options);

			$categories = $this->getElementsFromModel('list', 'Category', array());
			if (isset($this->request->data['Subcategory'][0]))
				$subcategories = $this->getElementsFromModel('list', 'Subcategory', array('category_id' => $this->request->data['Subcategory'][0]['category_id']));
			$this->set(compact('categories', 'subcategories'));
		}
	}

	public function getSubcategoryInput($category_id) {
		$this->layout = "ajax";

		$this->loadModel('Subcategory');
		$this->Subcategory->recursive = 1;
		$this->Subcategory->locale = $this->_getLanguage();
		$subcategories = $this->Subcategory->find('list', array('conditions' => array('category_id' => $category_id), 'order' => array('name' => 'ASC'))); // поиск подкатегорий первой для категории в списке
		$this->set(compact('subcategories'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->is_login())
			$this->logout();
		
		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Не найдена запрашиваемая компания'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Company->delete()) {
			// $this->Flash->success(__('Компания была удалена.'));
		} else {
			// $this->Flash->error(__('Не удалось удалить компанию. Пожалуйста, попробуйсте еще раз.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function togglePriority($id = null) {
		if (!$this->is_login())
			$this->logout();

		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Не найдена запрашиваемая компания'));
		}
		$this->Company->recursive = 1;
		$this->Company->locale = $this->_getLanguage();
		$priority = $this->Company->find('first', array('conditions' => array('Company.id' => $id)));

		$priority = $priority['Company']['priority'];
		$priority = ($priority == 0) ? 1 : 0;

		if ($this->Company->saveField('priority', $priority)) {
			// $this->Flash->success(__('Компания была успешно обновлена.'));
		} else {
			// $this->Flash->error(__('Не удалось обновить компанию. Пожалуйста, попробуйсте еще раз.'));
		}
		$this->redirect(array('action' => 'index'));
	}
}
