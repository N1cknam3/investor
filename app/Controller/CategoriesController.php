<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $paginate = array(
        'limit' => 25
    );

	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->Paginator->settings = $this->paginate;
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->logout();
			}
			$this->layout = "default_admin";
		}

		$this->Category->locale = $this->_getLanguage();
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('languages', $this->languages);
		$this->Category->recursive = 1;
		$this->set('categories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Не найдена запрашиваемая категория'));
		}
		$this->loadModel('Subcategory');
		$this->Subcategory->recursive = 1;
		$this->Subcategory->locale = $this->_getLanguage();
		$this->Paginator->settings += array(
	        'conditions' => array('Subcategory.category_id' => $id)
	    );
		$this->set('subcategories', $this->Paginator->paginate('Subcategory'));

		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
		$this->set('languages', $this->languages);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Category->create();

			foreach ($this->languages as $lang) {
				$this->Category->setLocale($lang);
				$this->Category->save($this->request->data);
			}

			return $this->redirect(array('action' => 'index'));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $lang = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Не найдена запрашиваемая категория'));
		}

		$this->Category->setLocale($lang);

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Category->save($this->request->data)) {
				// $this->Flash->success(__('Категория была успешно обновлена.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				// $this->Flash->error(__('Не удалось обновить категорию. Пожалуйста, попробуйсте еще раз.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Не найдена запрашиваемая категория'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Category->delete()) {
			// $this->Flash->success(__('Категория была удалена.'));
		} else {
			// $this->Flash->error(__('Не удалось удалить категорию. Пожалуйста, попробуйсте еще раз.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
