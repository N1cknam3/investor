<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	public $helpers = array('Html', 'Session');
	
	public $components = array(
    	'Paginator',
		'DebugKit.Toolbar',
		'Session',
		'Cookie'
	);
	
	const SESSION_USER = "userID";
	const ERR_ALL_FIELDS = 'Все поля обязательны для заполнения!';
	const ERR_LOGIN_PASS = "Неверное имя пользователя или пароль!";
	const ERR_FILE = "Недопустимый тип файла!";
	const SUCCESS_UPDATE = "Данные успешно сохранены!";
	const SUCCESS_FILE = "Файлы успешно загружены";
	const ERR_FILE_DEL = "Ошибка удаления файла";

    public $languages = array('rus', 'deu');

	public function beforeFilter() {
		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();
		$categories_global = $this->Category->find('list');
		$this->set(compact('categories_global'));

		$this->_setLanguage();
	}

	public function rus() {
		$this->Cookie->write('lang', 'rus', false, '20 days');
   		$this->redirect($this->referer());
	}

	public function deu() {
		$this->Cookie->write('lang', 'deu', false, '20 days');
   		$this->redirect($this->referer());
	}

	private function _setLanguage() {
	    if ($this->Cookie->check('lang')) {
	    	$this->Session->write('Config.language', $this->Cookie->read('lang'));
	    	Configure::write('Config.language', $this->Cookie->read('lang'));
	    }
    }

    protected function _getLanguage() {
	    if ($this->Cookie->check('lang')) {
	    	return $this->Cookie->read('lang');
	    }
	    return Configure::read('Config.language');
    }
	
	public function get_user_byLoginPass($login_to_check, $pass_to_check) {
		$this->loadModel('User');
		$user = $this->User->find('first', array('conditions' => array('login' => $login_to_check, 'password' => $pass_to_check)));
		return $user;
	}
	
	protected function login($login, $password) {
		$user = $this->getElementsFromModel('first', 'User', array('login' => $login, 'password' => $password));
		if (!empty($user)) {
			$this->Session->write(self::SESSION_USER, $user['User']['id']);
			$this->set('login_message', 'ok');
			$this->set('login_url', Router::fullbaseUrl().Router::url(array('controller' => 'admin', 'action' => 'index')));
		} else {
			$this->logout();
		}
	}
	
	protected function setSessionData($user_id) {
		$this->Session->write(self::SESSION_USER, $user_id);
	}
	
	public function logout() {
		$this->Session->write(self::SESSION_USER, null);
		$this->redirect('/');
	}
	
	public function ajax_login(){
		$this->layout = 'ajax';
		if(empty($this->request->data['login'])||empty($this->request->data['pass'])) {
			$this->set('login_message', self::ERR_ALL_FIELDS);
		} else {
			
			$user = $this->get_user_byLoginPass($this->request->data['login'], $this->request->data['pass']);
			
			if (!empty($user)) {			
				$this->setSessionData($user['User']['id']);
				$this->set('login_message', 'ok');
				//$this->set('login_url', Router::fullbaseUrl().Router::url(array('controller' => 'admins', 'action' => 'cabinet')));
				exit;
			} else {
				$this->set('login_message', self::ERR_LOGIN_PASS);
			}
		}		
	}
	
	public function is_login() {
		$user = $this->Session->read(self::SESSION_USER);
		return !empty($user);
	}
	
	//	Удаление элемента из модели
	protected function deleteElementFromModel($elem_id, $modelName) {
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден %s с ID=%d'), $modelName, $elem_id);
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->$modelName->delete()) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException(__('Невозможно удалить запись с ID=%d из "%s"!'), $elem_id, $modelName); // 500 error
		}
	}
	
	//	Создание нового элемента в модели $modelName с данными $data
	protected function createElementToModel($data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->create();
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return $this->$modelName->id;
		} else {
			//return false;
			throw new InternalErrorException(__('Невозможно сохранить данные в "%s"!'), $modelName); // 500 error
		}
	}
	
	//	Обновление данных элемента в модели $modelName данными $data
	protected function updateElementToModel($elem_id, $data, $modelName) {
		//	Примечание: $data не обязательно должно быть приведено к виду "array($modelName => array(..))"
		
		$this->loadModel($modelName);
		$this->$modelName->id = $elem_id;
		if (!$this->$modelName->exists()) {
			throw new NotFoundException(__('Не найден %s с ID=%d'), $modelName, $elem_id);
		}
		if (!array_key_exists($modelName, $data)) {
			$data = array(
				$modelName => $data
			);
		}
		if ($this->$modelName->save($data)) {
			return true;
		} else {
			//return false;
			throw new InternalErrorException(__('Невозможно обновить запись с ID=%d в "%s"!'), $elem_id, $modelName);
		}
	}
	
	protected function getElementsFromModel($findType, $modelName, $conditions, $other_options = null) {
		$this->loadModel($modelName);
		$options = array(
			'conditions' => $conditions
		);
		if ($other_options)
			$options += $other_options;
		return $this->$modelName->find($findType, $options);
	}
	
	//	Очистка уровня массива - имени модели
	protected function redunantModelName($data, $modelName) {
		return Set::extract('/'.$modelName.'/.', $data);
	}
}
