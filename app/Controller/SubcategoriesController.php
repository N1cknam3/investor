<?php
App::uses('AppController', 'Controller');
/**
 * Subcategories Controller
 *
 * @property Subcategory $Subcategory
 * @property PaginatorComponent $Paginator
 */
class SubcategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public $paginate = array(
        'limit' => 25
    );

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Paginator->settings = $this->paginate;
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->logout();
			}
			$this->layout = "default_admin";
		}

		$this->Subcategory->locale = $this->_getLanguage();
	}

/**
 * add method
 *
 * @return void
 */
	public function add($category_id = null) {
		if (!$category_id)
			$category_id = $this->request->data['Subcategory']['category_id'];

		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();
		$categories = $this->Category->find('list');
		$this->set(compact('categories', 'category_id'));

		if ($this->request->is('post')) {
			$this->Subcategory->create();
			
			foreach ($this->languages as $lang) {
				$this->Subcategory->setLocale($lang);
				$this->Subcategory->save($this->request->data);
			}

			return $this->redirect(array('controller' => 'categories', 'action' => 'view', $category_id));
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null, $lang = null) {
		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();
		$categories = $this->Category->find('list');
		$this->set(compact('categories'));
		
		if (!$this->Subcategory->exists($id)) {
			throw new NotFoundException(__('Не найдена запрашиваемая подкатегория'));
		}

		$this->Subcategory->setLocale($lang);

		if ($this->request->is(array('post', 'put'))) {
			if ($this->Subcategory->save($this->request->data)) {
				// $this->Flash->success(__('Подкатегория была успешно обновлена.'));
				return $this->redirect(array('controller' => 'categories', 'action' => 'view/'.$this->request->data['Subcategory']['category_id']));
			} else {
				// $this->Flash->error(__('Не удалось обновить подкатегорию. Пожалуйста, попробуйсте еще раз.'));
			}
		} else {
			$options = array('conditions' => array('Subcategory.' . $this->Subcategory->primaryKey => $id));
			$this->request->data = $this->Subcategory->find('first', $options);
			$this->set('category_id', $this->request->data['Subcategory']['category_id']);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Subcategory->id = $id;
		if (!$this->Subcategory->exists()) {
			throw new NotFoundException(__('Не найдена запрашиваемая подкатегория'));
		}
		$category_id = $this->Subcategory->find('first', array(
	        'conditions' => array('Subcategory.id' => $id)
	    ))['Subcategory']['category_id'];
		$this->request->allowMethod('post', 'delete');
		if ($this->Subcategory->delete()) {
			// $this->Flash->success(__('Подкатегория была удалена.'));
		} else {
			// $this->Flash->error(__('Не удалось удалить подкатегорию. Пожалуйста, попробуйсте еще раз.'));
		}
		return $this->redirect(array('controller' => 'categories', 'action' => 'view/'.$category_id));
	}
}
