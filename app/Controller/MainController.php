<?php

App::uses('AppController', 'Controller');

class MainController extends AppController {

	public $uses = array();
	public $helpers = array('Html', 'MainWidgets');

	public $components = array('Paginator');

	public $paginate = array(
        'limit' => 25
    );

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Paginator->settings = $this->paginate;
	}

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->layout = "default_with_slider";
			} else 
				$this->layout = "default_admin_with_slider";
		}
		
		$title_for_layout = __("Главная");

		$this->loadModel('Company');
		$this->Company->recursive = 1;
		$this->Company->locale = $this->_getLanguage();
		$companies = $this->Company->find('all', array('conditions' => array('priority' => 1)));
		$companies = $this->redunantModelName($companies, "Company");

		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();
		$categories = $this->Category->find('list');

		$this->set(compact('title_for_layout', 'companies', 'categories'));
	}
	
	public function search($search_type = "all", $category_id = null, $subcategory_id = null) {
		if ( !$this->request->is('ajax')) {
			if (!$this->is_login()) {
				$this->layout = "default";
			} else 
				$this->layout = "default_admin";
		}
		
		//	Включение показа рекламы
		$set_advertisement = false;
		$this->set(compact($set_advertisement, 'set_advertisement'));
		
		//	Список всех категорий для бокового меню поиска
		$this->loadModel('Category');
		$this->Category->recursive = 1;
		$this->Category->locale = $this->_getLanguage();

		$this->loadModel('Subcategory');
		$this->Subcategory->recursive = 1;
		$this->Subcategory->locale = $this->_getLanguage();

		$categories = $this->Category->find('all');

		foreach ($categories as $key => $category) {
			if (isset($category['Subcategory'])) {
				$tmp_res = $this->Subcategory->find('all', array('conditions' => array('category_id' => $category['Category']['id'])));
				foreach ($tmp_res as $tmp_res_key => $tmp_res_value) {
					$tmp_res[$tmp_res_key] = $tmp_res_value['Subcategory'];
				}

				$categories[$key]['Subcategory'] = $tmp_res;
			}
		}

		// $categories = $this->getElementsFromModel("all", "Category", array());
		$this->set(compact('categories'));

		$this->loadModel('Company');
		$this->Company->recursive = 1;
		$this->Company->locale = $this->_getLanguage();

		$search_conditions = array();
		if ($search_type == 'new') {
			$search_conditions = array(
				'Company.priority' => 1
			);
		}
		
		if ($category_id) {
			$search_conditions += array('Category.id' => $category_id);

			$choosed_category = $this->Category->find('first', array('conditions' => array('Category.id' => $category_id)));

			// $choosed_category = $this->getElementsFromModel("first", "Category", array('Category.id' => $category_id));
			$this->set(compact('choosed_category'));
		}
		
		if ($subcategory_id) {
			$search_conditions += array('Subcategory.id' => $subcategory_id);

			$choosed_subcategory = $this->Subcategory->find('first', array('conditions' => array('Subcategory.id' => $subcategory_id)));
			
			// $choosed_subcategory = $this->getElementsFromModel("first", "Subcategory", array('Subcategory.id' => $subcategory_id));
			$this->set(compact('choosed_subcategory'));
		}

		$paginate_settings = $this->getCompanyPaginateSettings();
		$paginate_settings['conditions'] = $search_conditions;
		$this->Paginator->settings += $paginate_settings;
		$result_data = $this->Paginator->paginate('Company');

		foreach ($result_data as $key => $company) {
			$res = $this->Subcategory->find('first', array('conditions' => array('Subcategory.id' => $company['Subcategory']['id'])));
			$result_data[$key]['Subcategory'] = $res['Subcategory'];
			$res = $this->Category->find('first', array('conditions' => array('Category.id' => $company['Category']['id'])));
			$result_data[$key]['Category'] = $res['Category'];
		}

		$this->set(compact('result_data', 'category_id', 'subcategory_id'));
	}

	public function search_result() {
		if (!$this->request->is('post')) {
			$this->redirect('/');
		}

		//	Список всех категорий для бокового меню поиска
		$categories = $this->getElementsFromModel("all", "Category", array());
		$search_result = true;
		$this->set(compact('categories', 'search_result'));

		$key = $this->request->data['Search']['keywords'];

		$this->loadModel('Company');

		// костыль, необходимо переделать вывод данных
		$search_conditions = array('OR' => array(
		    array('Company.name LIKE' => "%$key%"),
		    array('Company.short_desc LIKE' => "%$key%"),
		    array('Company.full_desc LIKE' => "%$key%"),
		    array('Company.contacts LIKE' => "%$key%"),
		    array('Company.address LIKE' => "%$key%")
		));

		$paginate_settings = $this->getCompanyPaginateSettings();
		$paginate_settings['conditions'] = $search_conditions;
		$this->Paginator->settings += $paginate_settings;
		$result_data = $this->Paginator->paginate('Company');
		$this->set(compact('result_data'));
	}

	private function getCompanyPaginateSettings() {

		// SELECT companies.*, subcategories.*, categories.*
		// FROM companies
		// JOIN companies_subcategories ON companies.id = companies_subcategories.company_id
		// JOIN subcategories ON companies_subcategories.subcategory_id = subcategories.id
		// JOIN categories ON subcategories.category_id = categories.id

		return array(
		    'joins' => array(
		        array(
		            'table' => 'companies_subcategories',
		            'alias' => 'CompaniesSubcategories',
		            'type' => 'INNER',
		            'conditions' => array(
		                'Company.id = CompaniesSubcategories.company_id'
		            )
		        ),
		        array(
		            'table' => 'subcategories',
		            'alias' => 'Subcategory',
		            'type' => 'INNER',
		            'conditions' => array(
		                'CompaniesSubcategories.subcategory_id = Subcategory.id'
		            )
		        ),
		        array(
		            'table' => 'categories',
		            'alias' => 'Category',
		            'type' => 'INNER',
		            'conditions' => array(
		                'Subcategory.category_id = Category.id'
		            )
		        ),
		    ),
		    'fields' => array('Company.*', 'Subcategory.*', 'Category.*')
		);
	}

	private function fetchTranslate($data, $modelName) {

	}
}
