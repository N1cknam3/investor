<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

	public function setLocale($lang) {
		if ($lang)
			$this->locale = $lang;
	}

	/*  
	 * See http://book.cakephp.org/2.0/en/core-libraries/behaviors/translate.html#using-the-bindtranslation-method
	 * This is for making it so we fetch all translations, as opposed to just that of the current locale.
	 * Used for eg. editing (multiple translations) via the admin interface.
	 */
	public function bindAllTranslations() {
	    $translatableFields = $this->actsAs['Translate'];

	    $keyValueFields = array();
	    foreach($translatableFields as $field){
	        $keyValueFields[$field] = $field.'Translation';
	    }

	    $this->bindTranslation($keyValueFields,false);  // false means it will be changed for all future DB transactions in this page request - and won't be reset after the next transaction.
	}

	/**
     * copy the HABTM post value in the data validation scope
     * from data[distantModel][distantModel] to data[model][distantModel]
     * @return bool true
     */
	public function beforeValidate($options = array()){
	    foreach (array_keys($this->hasAndBelongsToMany) as $model){
	    	if(isset($this->data[$model][$model]))
	    		$this->data[$this->name][$model] = $this->data[$model][$model];
	 	}

	   	return true;
	}

	/**
	 * delete the HABTM value of the data validation scope (undo beforeValidate())
	 * and add the error returned by main model in the distant HABTM model scope
	 * @return bool true
	 */
	public function afterValidate($options = array()){
	   foreach (array_keys($this->hasAndBelongsToMany) as $model){
	    	unset($this->data[$this->name][$model]);
	    	if(isset($this->validationErrors[$model]))
	    		$this->$model->validationErrors[$model] = $this->validationErrors[$model];
	   	}

	   	return true;
	}
}
