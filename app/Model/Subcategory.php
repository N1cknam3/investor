<?php
App::uses('AppModel', 'Model');
/**
 * Subcategory Model
 *
 */
class Subcategory extends AppModel {

	public $actsAs = array(
		'Containable',
		'Translate' => array(
            'name'
        )
	);
	
	public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id'
        )
    );
    
    public $hasAndBelongsToMany = array(
        'Company' =>
            array(
                'className' => 'Company',
                'joinTable' => 'companies_subcategories',
                'foreignKey' => 'subcategory_id',
                'associationForeignKey' => 'company_id',
                'unique' => true
            )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'id_category' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
