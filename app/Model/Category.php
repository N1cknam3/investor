<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 */
class Category extends AppModel {

	public $actsAs = array(
		'Containable',
		'Translate' => array(
            'name'
        )
	);
	
	public $hasMany = array(
        'Subcategory' => array(
            'className' => 'Subcategory',
            'foreignKey' => 'category_id',
            //'conditions' => array('Subcategory.status' => '1'),
            //'order' => 'Subcategory.created DESC',
            //'limit' => '5',
            'dependent' => true
        )
    );

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
