function login_success(data) {
    if (data) {
        $("#status").html(data);
        $("#status").show();
        animateElement1("#status", message_animation_effect);
    } else {
        $("#status").hide();
        location.reload();
    }
};

/*
    Функция проверки полей ввода формы
    При пустом data переходит по пути urlPathб,
    иначе показывает элемент elementSelector (сообщение об ошибке)
*/
function form_success(data, elementSelector, urlPath) {
    if (data) {
        $(elementSelector).html(data);
        $(elementSelector).show();
        animateElement1(elementSelector, message_animation_effect);
    } else {
        $(elementSelector).hide();
        location.href = $.url(urlPath);
    }
};

/*
    Отдает input и присваивает его выбранному модификатору
 */
function load_input (urlPath) {
    var $element = $(event.target);
    $.ajax({
        type: "POST",
        url: $.url(urlPath),
        
        beforeSend: function(){
            //  Анимация загрузки
            //$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        },
        
        success: function(data){
            // alert(data);
            $element.closest(".category-tag").find(".additional").html(data);
            // call template js
            $.fn.initializeSelects();
        }
    });
}